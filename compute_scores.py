# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 18:39:05 2021

@author: Gaspard
"""

from PatchMatch import NNS, show_offset
import glob
import numpy as np
from PIL import Image
import tqdm

list_image_names = ["blotchy","bubbly","cracked1","cracked2","fibrous1","fibrous2","grass","honeycomb","marbled","smeared","stratified","wrinkled"]
orig_path = "Images/square/split/"
fake_path = "Images/fake_samples/"

image_name = "cracked2"
p_size = 11 #patch size
itr = 5 #number of patchmatch iteration
nb_fake_image = 3 #inferior to 10
zoom = 1 #center crop

fake_images = glob.glob(f"{fake_path}/{image_name}_0/*/*.png")
orig_image = glob.glob(f"{orig_path}/{image_name}*.jpg")

orig = np.array(Image.open(orig_image[0]))

h = orig.shape[0]
z_min = h//2 - h//(2*zoom)
z_max = h//2 + h//(2*zoom)

orig = orig[z_min:z_max,z_min:z_max,:3]

orig_scores = np.zeros(3)
for i in tqdm.tqdm(range(3)):
    img = np.array(Image.open(orig_image[i+1]))[z_min:z_max,z_min:z_max,:3]
    fi,fj, dist, score_list = NNS(orig, img, p_size, itr,False)
    orig_scores[i] = score_list[-1]
    
fake_scores = np.zeros(nb_fake_image)
for i in tqdm.tqdm(range(nb_fake_image)):
    img = np.array(Image.open(fake_images[i]))[z_min:z_max,z_min:z_max,:3]
    fi,fj, dist, score_list = NNS(orig, img, p_size, itr,False)
    fake_scores[i] = score_list[-1]
    
print(f"\nTrue scores mean : {int(orig_scores.mean())}\nFake scores mean : {int(fake_scores.mean())}")

show_offset(fi,fj,p_size)