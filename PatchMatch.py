import numpy as np
from PIL import Image
from numba import jit
import matplotlib.pyplot as plt
import colorsys

#cette fonction est tres souvent appelée
@jit(nopython=True)
def cal_distance(a, b, A_padding, B_padding, p_size):
    patch_a = A_padding[a[0]:a[0]+p_size, a[1]:a[1]+p_size, :]
    patch_b = B_padding[b[0]:b[0]+p_size, b[1]:b[1]+p_size, :]
    temp = patch_b - patch_a
    num = 0
    dist = 0
    for i in range(p_size):
        for j in range(p_size):
            for k in range(3):
                if not np.isnan(temp[i,j,k]):
                    num +=1 
                    dist += np.square(temp[i,j,k])
    dist /= num
    return dist

def reconstruction(fi,fj, A, B):
    A_h = np.size(A, 0)
    A_w = np.size(A, 1)
    temp = np.zeros_like(A)
    for i in range(A_h):
        for j in range(A_w):
            temp[i, j, :] = B[fi[i, j], fj[i, j], :]
    Image.fromarray(temp).show()
    
def initialization(A, B, p_size):
    A_h = np.shape(A)[0]
    A_w = np.shape(A)[1]
    B_h = np.shape(B)[0]
    B_w = np.shape(B)[1]
    p = p_size // 2
    random_B_r = np.random.randint(0, B_h-1, [A_h, A_w])
    random_B_c = np.random.randint(0, B_w-1, [A_h, A_w])
    A_padding = np.ones([A_h+p*2, A_w+p*2, 3]) * np.nan
    A_padding[p:A_h+p, p:A_w+p, :] = A
    
    B_padding = np.ones([B_h+p*2, B_w+p*2, 3]) * np.nan
    B_padding[p:B_h+p, p:B_w+p, :] = B
    
    fi = np.zeros([A_h, A_w],dtype=int)
    fj = np.zeros([A_h, A_w], dtype=int)
    dist = np.zeros([A_h, A_w])
    for i in range(A_h):
        for j in range(A_w):
            a = np.array([i, j])
            b = np.array([random_B_r[i, j], random_B_c[i, j]], dtype=np.int32)
            fi[i, j] = random_B_r[i, j]
            fj[i, j] = random_B_c[i, j]
            dist[i, j] = cal_distance(a, b, A_padding, B_padding, p_size)
    return fi,fj, dist, A_padding, B_padding

@jit(nopython=True)
def random_search(fi,fj, a, dist, A_padding, B_padding, p_size, alpha=0.5):
    x = a[0]
    y = a[1]
    p = p_size // 2
    B_h = np.shape(B_padding)[0]-2*p
    B_w = np.shape(B_padding)[1]-2*p
    i = 4
    search_h = B_h * alpha ** i
    search_w = B_w * alpha ** i
    b_x = fi[x, y]
    b_y = fj[x, y]
    alpha=alpha
    while search_h > 1 and search_w > 1:
        search_min_r = max(b_x - search_h, 0)
        search_max_r = min(b_x + search_h, B_h-1)
        random_b_x = np.random.randint(search_min_r, search_max_r)
        search_min_c = max(b_y - search_w, 0)
        search_max_c = min(b_y + search_w, B_w-1)
        random_b_y = np.random.randint(search_min_c, search_max_c)
        search_h = B_h * alpha ** i
        search_w = B_w * alpha ** i
        b = np.array([random_b_x, random_b_y])
        d = cal_distance(a, b, A_padding, B_padding, p_size)
        if d < dist[x, y]:
            dist[x, y] = d
            fi[x, y] = b[0]
            fj[x, y] = b[1]
        i += 1
        
@jit(nopython=True)
def propagation(fi,fj, a, dist, A_padding, B_padding, p_size, is_odd):
    A_h = np.shape(A_padding)[0] - p_size + 1
    A_w = np.shape(A_padding)[1] - p_size + 1
    x = a[0]
    y = a[1]
    if is_odd:
        i_left,j_left = fi[max(x - 1, 0), y],fj[max(x - 1, 0), y]
        i_left = min(i_left+1,A_h-1)
        b_left = np.array([i_left,j_left],dtype="int64")
        d_left = cal_distance(a,b_left, A_padding, B_padding, p_size)
        
        i_up,j_up = fi[x,max(y - 1, 0)],fj[x, max(y - 1, 0)]
        j_up = min(j_up+1,A_w-1)
        b_up = np.array([i_up,j_up],dtype="int64")
        d_up = cal_distance(a, b_up, A_padding, B_padding, p_size)
        
        d_current = dist[x, y]
        idx = np.argmin(np.array([d_current, d_left, d_up]))
        if idx == 1:
            fi[x, y] = i_left
            fj[x, y] = j_left
            dist[x, y] = d_left
        if idx == 2:
            fi[x, y] = i_up
            fj[x, y] = j_up
            dist[x, y] = d_up
    else:
        i_right,j_right = fi[min(x + 1, A_h-1), y],fj[min(x + 1, A_h-1), y]
        i_right = max(i_right-1,0)
        b_right = np.array([i_right,j_right],dtype="int64")
        d_right = cal_distance(a, b_right, A_padding, B_padding, p_size)
        
        i_down,j_down = fi[x, min(y + 1, A_w-1)],fj[x, min(y + 1, A_w-1)]
        j_down = max(j_down-1,0)
        b_down = np.array([i_down,j_down],dtype="int64")
        d_down = cal_distance(a, b_down, A_padding, B_padding, p_size)
        
        d_current = dist[x, y]
        idx = np.argmin(np.array([d_current, d_right, d_down]))
        if idx == 1:
            fi[x, y] = i_right
            fj[x, y] = j_right
            dist[x, y] = d_right
        if idx == 2:
            fi[x, y] = i_down
            fj[x, y] = j_down
            dist[x, y] = d_down
            
def NNS(img, ref, p_size, itr, verbose=True):
    A_h = np.size(img, 0)
    A_w = np.size(img, 1)
    if verbose:
        print("initialization")
    fi,fj, dist, img_padding, B_padding = initialization(img, ref, p_size)
    score_list = []
    for itr in range(1, itr+1):
        if itr % 2 == 0:
            for i in range(A_h - 1, -1, -1):
                for j in range(A_w - 1, -1, -1):
                    a = np.array([i, j])
                    propagation(fi,fj, a, dist, img_padding, B_padding, p_size, False)
                    random_search(fi,fj, a, dist, img_padding, B_padding, p_size)
        else:
            for i in range(A_h):
                for j in range(A_w):
                    a = np.array([i, j])
                    propagation(fi,fj, a, dist, img_padding, B_padding, p_size, True)
                    random_search(fi,fj, a, dist, img_padding, B_padding, p_size)
        if verbose:
            print(f"Iter : {itr}, score : {dist.mean()}")
        score_list.append(dist.mean())
    return fi,fj, dist, score_list


def show_offset(fi,fj,p_size):
    di = np.zeros(fi.shape)
    dj = np.zeros(fi.shape)
    for i in range(fi.shape[0]):
        for j in range(fi.shape[1]):
            di[i,j] = i
            dj[i,j] = j
    offset_i = (fi-di)
    offset_j = (fj-dj)
    
    HSV = np.ones((offset_i.shape[0],offset_i.shape[1],3))
    for i in range(offset_i.shape[0]):
        for j in range(offset_i.shape[1]):
            x,y = offset_i[i,j]+10**-7,offset_j[i,j]+10**-7
            HSV[i,j][0] = (np.sqrt(x**2+y**2) + np.pi/2)/(2*np.pi)
            HSV[i,j][1] = (np.pi*(x<0) + np.arctan(y/x) + np.pi/2)/(2*np.pi)
            
    HSV[:,:,0] /= HSV[:,:,0].max()
    
    RGB = np.zeros(HSV.shape)
    for i in range(offset_i.shape[0]):
        for j in range(offset_i.shape[1]):
            h,s,v = HSV[i,j,0] , HSV[i,j,1], 1
            RGB[i,j,:] = colorsys.hsv_to_rgb(h, s, v)
    
    plt.imshow(RGB)
    plt.show()
    
if __name__ == "__main__":
    orig = np.array(Image.open("cracked2_0.jpg"))
    fake = np.array(Image.open("cracked2_0_fake.png"))[:,:,:3]
    true = np.array(Image.open("cracked2_1.jpg"))
    
    p_size = 11
    itr = 10
    
    fi,fj, dist, score_list_fake = NNS(orig, fake, p_size, itr)
    show_offset(fi,fj,p_size)
    
    fi,fj, dist, score_list = NNS(orig, true, p_size, itr)
    show_offset(fi,fj,p_size)
    
    plt.plot(score_list_fake,label="fake")
    plt.plot(score_list,label="true")
    plt.xlabel("iterations")
    plt.ylabel("distance")
    plt.title("distance between the original sample and the fake/true")
    plt.legend()
    plt.show()
    
    reconstruction(fi,fj, orig, true)
    
    
    
    
    